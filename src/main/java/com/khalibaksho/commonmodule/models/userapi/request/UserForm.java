package com.khalibaksho.commonmodule.models.userapi.request;

import lombok.Data;

@Data
public class UserForm {
  private String name;
  private String email;
  private String password;
}
