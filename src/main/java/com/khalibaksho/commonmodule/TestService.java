package com.khalibaksho.commonmodule;

import org.springframework.stereotype.Service;

@Service
public class TestService {
    public String test(){
        return "Test Service Called";
    }
}
