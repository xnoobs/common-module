package com.khalibaksho.commonmodule.models.userapi.response;

import lombok.Builder;
import lombok.Data;

/**
 * User response model
 *
 * @author sarouarhossain
 */
@Data
@Builder
public class UserResponse {
  private String id;
  private String name;
  private String email;
}
