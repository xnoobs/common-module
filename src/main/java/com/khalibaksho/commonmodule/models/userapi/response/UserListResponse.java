package com.khalibaksho.commonmodule.models.userapi.response;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * User list response model
 *
 * @author sarouarhossain
 */
@Data
@Builder
public class UserListResponse {
  private List<UserResponse> users;
}
